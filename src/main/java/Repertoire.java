import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

/**
 * Classe Repertoire, gère un ensemble de documents selon le pattern Composite
 * Parcours de l'ensemble selon le pattern Iterator.
*/
public class Repertoire extends Document implements Iterable<Document> {
	
	private Repertoire parent;
	private List<Document> contenu;
	
	/**
	 * Constructeur de la classe
	*/
	public Repertoire (String nom,  Repertoire parent) {
		this.nom = nom;
		this.parent = parent;
		contenu = new ArrayList<Document>();
	}
	
	public void add(Document d) {
		contenu.add(d);
	}
	
	public Repertoire getRepertoire(String nom) {
		for ( int i = 0; i < contenu.size(); i++ ) {
			Document d = contenu.get(i);
			if ( d.getNom().equals(nom) && d instanceof Repertoire ) {
				return (Repertoire) d;
			}
		}
		return null;
	}
	
	public Fichier getFichier(String nom) {
		for ( int i = 0; i < contenu.size(); i++ ) {
			Document d = contenu.get(i);
			if ( d.getNom().equals(nom) && d instanceof Fichier ) {
				return (Fichier) d;
			}
		}
		return null;
	}
	
	public Repertoire getParent () {
		return parent;
	}
	
	@Override
	public Iterator<Document> iterator() {
		return contenu.iterator();
	}
}
