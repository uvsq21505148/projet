import java.util.ArrayList;

/**
 * Class CommandAjouter qui permet d'ajouter les fichier indiqué par les chemins à l'INBOX.
 */

public class CommandAjouter implements Command {
	
	private boolean option_auto; // option : "-auto"
	private ArrayList<String> repositories;
	private ArrayList<String> files;
	
	public CommandAjouter() {
		option_auto = false;
		repositories = new ArrayList<String>();
		files = new ArrayList<String>();
	}
	
	/**
	 * Methode setOptionAuto qui met à jours l'attribut option_auto si il
	 * a été précisé dans la commande.
	 * 
	 * @param b 
	 * 		Vaut true ou false si il a été oui ou non écrit quand l'utilisateur saisi la commande.
	 */
	public void setOptionAuto(boolean b) {
		option_auto = b;
	}
	
	/**
	 * Methode setRepositories qui ajoute le nom du répertoire à la liste repositories.
	 * 
	 * @param rep
	 * 		Nom du répertoire que l'on veut ajouter.
	 */
	public void setRepositories(String rep){
		
		repositories.add(rep);
	}
	
	/**
	 * Methode setFiles qui ajoute le nom du fichier à la liste files.
	 * 
	 * @param fic
	 * 		Nom du fichier que l'on veut ajouter
	 */
	public void setFiles(String fic){
		
		files.add(fic);
	}
	
	public boolean getOptionAuto(){
		return this.option_auto;
	}
	
	public ArrayList<String> getRepositories(){
		return this.repositories;
	}
	
	public ArrayList<String> getFiles(){
		return this.files;
	}
	
	/**
	 * Methode apply qui applique la commande.
	 */
	@Override
	public void apply() {
		AnalyseurArguments.argsCommandAjouter(this);
		
		//Fermeture de l'application
		if(option_auto) {
			System.out.println("Ajout d'un fichier à l'INBOX avec option auto");
		}
		else {
			System.out.println("Ajout d'un fichier à l'INBOX sans option auto");
		}
			
	}
}
