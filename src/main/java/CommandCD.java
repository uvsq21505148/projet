import java.util.Arrays;

/**
 * Classe CommandCD permet de changer de répertoire courant
*/
public class CommandCD implements Command {
	
	private String nom = "";
	
	public void setNom (String nom) {
		this.nom = nom;
	}
	
	@Override
	public void apply () {
		AnalyseurArguments.argsCommandCD (this);
		
		if ( nom.equals("..") && ! Application.ENVIRONNEMENT.path.equals("/") ) {
			String[] paths = Application.ENVIRONNEMENT.path.split("/");
			String path = "";
			for(int i = 0; i < paths.length - 1; i++) path += paths[i] + "/";
			Application.ENVIRONNEMENT.path =  path;
			Application.ENVIRONNEMENT.courant = Application.ENVIRONNEMENT.courant.getParent();
			
		}
		else if ( ! nom.equals("") ) {
			Repertoire repertoire = Application.ENVIRONNEMENT.courant.getRepertoire(nom);
			
			if ( repertoire != null ) {
				Application.ENVIRONNEMENT.path += nom + "/";
				Application.ENVIRONNEMENT.courant = repertoire;
			}
			else {
				System.out.println("Aucun nom de dossier trouvé.");
			}
		}
		else {
			System.out.println("Aucun nom de dossier trouvé.");
		}
	}
}
