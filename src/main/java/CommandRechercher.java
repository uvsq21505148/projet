import java.util.ArrayList;

/**
 * Class CommandRechercher qui liste les fichier du répertoire courant contenant la chaine de caractère spécifié.
 * 
 */
public class CommandRechercher implements Command {
	
	private boolean option_recursive; 	// option : "-r"
	private boolean option_inbox; 		// option : "-inbox"
	private boolean option_tag;			// option : "[tag=nom]"
	private ArrayList<String> tags;
	
	public CommandRechercher() {
		option_recursive = false;
		option_inbox = false;
		option_tag = false;
		tags = new ArrayList<String>();
	}
	
	/**
	 * Methode setOptionRecursive qui met à jours l'attribut option_recursive.
	 * 
	 * @param b
	 *		Vaut true ou false si l'utilisateur à oui ou non spécifié cette option lors de la saisie.
	 */
	public void setOptionRecursive(boolean b) {
		option_recursive = b;
	}
	
	/**
	 * Methode setOptionInbox qui met à jours l'attribut option_recursive.
	 * 
	 * @param b
	 *		Vaut true ou false si l'utilisateur à oui ou non spécifié cette option lors de la saisie.
	 */
	public void setOptionInbox(boolean b) {
		option_inbox = b;
	}
	
	/**
	 * Methode setOptionTag qui met à jours l'attribut option_recursive.
	 * 
	 * @param b
	 *		Vaut true ou false si l'utilisateur à oui ou non spécifié cette option lors de la saisie.
	 */
	public void setOptionTag(boolean b) {
		option_tag = b;
	}
	
	/**
	 * Methode addTag qui ajoute un ou plusieurs tags spécifié par l'utilisateur à la liste tags.
	 * 
	 * @param nom
	 *		Nom du tag qu'on ajoute à la liste tags.
	 */
	public void addTag(String nom){
		tags.add(nom);
	}
	
	public boolean getOptionRecursive(){
		return this.option_recursive;
	}
	
	public boolean getOptionInbox(){
		return this.option_inbox;
	}
	
	public boolean getOptionTag(){
		return this.option_tag;
	}
	
	public ArrayList<String> getTag(){
		return this.tags;
	}
	
	/**
	 * Methode apply qui applique la commande.
	 * 
	 */
	@Override
	public void apply() {
		AnalyseurArguments.argsCommandRechercher(this);
		
		if(option_recursive){
			System.out.println("Recherche de fichier avec option recursive");
		}else{
			System.out.println("Recherche de fichier sans option recursive");
		}
		
		if(option_inbox){
			System.out.println("Recherche de fichier sur l'integralité des fichiers");
		}else{
			System.out.println("Recherche de fichier uniquement sur le repertoire courant");
		}
		
		if(option_tag){
			System.out.println("Recherche de fichier comportant un ou plusieur tag " + tags);
		}else{
			System.out.println("Recherche de fichier sans précision de tag");
		}
			
	}
}
