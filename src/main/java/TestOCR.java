import com.asprise.ocr.Ocr;
import java.io.File;

public class TestOCR {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Ocr.setUp();
		Ocr ocr = new Ocr(); // create a new OCR engine
		ocr.startEngine("fra", Ocr.SPEED_FASTEST); // English
		String s = ocr.recognize(new File[] {new File("Citation.png")}, Ocr.RECOGNIZE_TYPE_ALL, Ocr.OUTPUT_FORMAT_PLAINTEXT);
		System.out.println("Result: " + s);
		// ocr more images here ...
		ocr.stopEngine();
	}

}
