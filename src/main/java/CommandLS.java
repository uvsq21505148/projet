/**
 * Classe CommandLS pour afficher les éléments du dossier courant 
*/
public class CommandLS implements Command {
	
	private boolean option_inbox;
	
	public void setOptionInbox ( boolean b ) {

		option_inbox = b;
	}
	
	public CommandLS () {
		option_inbox = false;
	}
	
	@Override
	public void apply() {
		AnalyseurArguments.argsCommandLS(this);
		
		if( option_inbox ) {
			
			for(Document d : Application.ENVIRONNEMENT.INBOX)
				Print.printWhite(d.getNom() + " ");
				
			Print.printWhiteln("");
		}
		else {
			for(Document d : Application.ENVIRONNEMENT.courant) {

				if(d instanceof Repertoire) {
					Print.printBlue(d.getNom() + " ");
				}
				else {
					Print.printWhite(d.getNom() + " ");
				}
			}
			
			Print.printWhiteln("");
		}	
	}
}
