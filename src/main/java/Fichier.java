import java.util.List;
import java.util.ArrayList;

/**
 * Classe Fichier, initialisée à l'aide du pattern Builder
*/
public class Fichier extends Document {
	
	
	private String contenu;
	private List<String> tags;
	
	/**
	 * Constructeur de la classe
	*/
	private Fichier (Builder b) {
		this.nom = b.nom;
		this.contenu = b.contenu;
		this.tags = b.tags;
	}
	
	public static class Builder {
		
		private String nom;
		private String contenu;
		private List<String> tags;
		
		public Builder( String nom, String contenu ) {
			this.nom = nom;
			this.contenu = contenu;
			tags = new ArrayList<String>();
		}
		
		public Builder addTag(String tag) {
			tags.add(tag);
			return this;
		}
		
		public Fichier build() {
			return new Fichier(this);
		}
	}
	
	public void addTag(String tag) {
		tags.add(tag);
	}
	
	public boolean containsTag ( String tag ) {
		for( String s : tags ) {
			if ( s.equals(tag) ) {
				return true;
			}
		}
		return false;
	}
	
	public String getPath() {
		return "./INBOX/" + nom;
	}
	
	public String getContenu () {
		return contenu;
	}
}
