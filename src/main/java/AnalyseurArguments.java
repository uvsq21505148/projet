import java.util.Arrays;
import java.io.File;

/**
 * Class AnalyseurArguments qui va analyser les différents arguments en fonction
 * de la commande saisie par l'utilisateur.
 */
public class AnalyseurArguments {
	
	public static void argsCommandQuit(CommandQuit cmd) {
		
		String[] args = SaisieUtilisateur.getArgs();
		
		//Gérer les arguments et les options
		
		Arrays.stream(args)
			.filter(arg -> arg.compareTo("") != 0 && arg.charAt(0) == '-')
			.forEach(arg -> {
				switch(arg) {
					case "-joli" : cmd.setOptionJoli(true); break;
					default: break;
				}
			});
	}
	
	public static void argsCommandAjouter(CommandAjouter cmd) {
		
		String[] args = SaisieUtilisateur.getArgs();
			
		//Gérer les arguments et les options
				
		Arrays.stream(args)
			.filter(arg -> arg.compareTo("") != 0 && arg.charAt(0) == '-')
			.forEach(arg -> {
				switch(arg) {
					case "-auto" : cmd.setOptionAuto(true); break;
					default: break;
				}
			});
			
		try{
		//Gestion des Repertoires et des Fichiers
		Arrays.stream(args)
			.filter(arg -> arg.compareTo("") != 0 && arg.charAt(0) != '-')
			.forEach(arg -> {
				File f = new File(arg);
				if(f.isFile()){
					cmd.setFiles(arg);
				}else if(f.isDirectory()){
					cmd.setRepositories(arg);
				}else{
					System.out.println("L'argument \"" + arg + "\" est ni un dossier ni un fichier");
				}
			});
		}catch(Exception e){
			
			e.printStackTrace();
		}
	}
	
	public static void argsCommandRechercher(CommandRechercher cmd) {
		
		String[] args = SaisieUtilisateur.getArgs();
			
		//Gérer les arguments et les options		
		
		Arrays.stream(args)
			.filter(arg -> arg.compareTo("") != 0 && arg.charAt(0) == '-')
			.forEach(arg -> {
				switch(arg) {
					case "-r" : cmd.setOptionRecursive(true); break;
					case "-inbox" : cmd.setOptionInbox(true); break;
					default: break;
				}
			});
			
		try{
		//Gestion des tags
		Arrays.stream(args)
			.filter(arg -> arg.compareTo("") != 0 && arg.startsWith("[tag=") == true)
			.forEach(arg -> {
				String nom = new String();
				cmd.setOptionTag(true);
				nom =arg.substring(5,arg.length()-1);
				//System.out.println(nom);
				cmd.addTag(nom);
			});
		}catch(Exception e){
			
			e.printStackTrace();
		}
	}
	
	public static void argsCommandRechercheTxt(CommandRechercheTxt cmd) {
		
		String[] args = SaisieUtilisateur.getArgs();	
		
		
		Arrays.stream(args)
			.filter(arg -> arg.compareTo("") != 0 && arg.charAt(0) == '-')
			.forEach(arg -> {
				switch(arg) {
					case "-inbox" : cmd.setOptionInbox(true); break;
					case "-virtual" : cmd.setOptionInbox(false); break;
					default: break;
				}
			});
			
			
		for( int i = 0; i < args.length; i++ ) {
			if( args[i].charAt(0) != '-' ) {
				cmd.setMotCle(args[i]);
				break;
			}
		}
			
	}
	
	public static void argsCommandMkdir (CommandMkdir cmd) {
		
		String[] args = SaisieUtilisateur.getArgs();
		
		if (args.length > 0) {
			cmd.setNom(args[0]);
		}
	}
	
	public static void argsCommandCD (CommandCD cmd) {
		
		String[] args = SaisieUtilisateur.getArgs();
		
		if (args.length > 0) {
			cmd.setNom(args[0].split(" ")[0]);
		}
	}
	
	public static void argsCommandAddFile (CommandAddFile cmd) {
		
		String[] args = SaisieUtilisateur.getArgs();
		
		Arrays.stream(args)
			.filter(arg -> arg.compareTo("") != 0 && arg.charAt(0) == '-')
			.forEach(arg -> {
				switch(arg) {
					case "-inbox" : cmd.setOptionInbox(true); break;
					case "-virtual" : cmd.setOptionInbox(false); break;
					default: break;
				}
			});
		
		for( int i = 0; i < args.length; i++ ) {
			if( args[i].charAt(0) != '-' ) {
				cmd.setChemin(args[i]);
				break;
			}
		}
	}
	
	public static void argsCommandLS(CommandLS cmd) {
		
		String[] args = SaisieUtilisateur.getArgs();
			
		//Gérer les arguments et les options		
		
		Arrays.stream(args)
			.filter(arg -> arg.compareTo("") != 0 && arg.charAt(0) == '-')
			.forEach(arg -> {
				switch(arg) {
					case "-inbox" : cmd.setOptionInbox(true); break;
					case "-virtual" : cmd.setOptionInbox(false); break;
					default: break;
				}
			});
	}
	
	public static void argsCommandAjoutTag(CommandAjoutTag cmd) {
		
		String[] args = SaisieUtilisateur.getArgs();
		
		if(args.length > 0){
			cmd.setFichier(args[0]);
			cmd.setTag(args[1]);
		}
	}
	
	public static void argsCommandRechercheTag(CommandRechercheTag cmd) {
		
		String[] args = SaisieUtilisateur.getArgs();
		
		if(args.length > 0){
			cmd.setTag(args[0]);
		}
	}
}
