import org.fusesource.jansi.AnsiConsole;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;


public class Print{

	public static void printGreen(String chaine){
		AnsiConsole.systemInstall();
        System.out.print( ansi().render("@|green " + chaine + "|@") );
        AnsiConsole.systemUninstall();
		
	}
	
	public static void printBlue(String chaine){
		AnsiConsole.systemInstall();
        System.out.print( ansi().render("@|cyan " + chaine + "|@") );
        AnsiConsole.systemUninstall();
	}
	
	public static void printWhite(String chaine){
		AnsiConsole.systemInstall();
        System.out.print(chaine);
        AnsiConsole.systemUninstall();
	}
	
	public static void printGreenln(String chaine){
		AnsiConsole.systemInstall();
        System.out.println( ansi().render("@|green " + chaine + "|@") );
        AnsiConsole.systemUninstall();
		
	}
	
	public static void printBlueln(String chaine){
		AnsiConsole.systemInstall();
        System.out.println( ansi().render("@|cyan " + chaine + "|@") );
        AnsiConsole.systemUninstall();
	}
	
	public static void printRedln(String chaine){
		AnsiConsole.systemInstall();
        System.out.println( ansi().render("@|red " + chaine + "|@") );
        AnsiConsole.systemUninstall();
	}
	
	public static void printWhiteln(String chaine){
		AnsiConsole.systemInstall();
        System.out.println(chaine);
        AnsiConsole.systemUninstall();
	}
}
