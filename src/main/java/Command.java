/**
 * L'interface Command qui implemente la méthode apply, qui sera
 * réécrite par les différentes commandes.
 * 
 */
@FunctionalInterface
public interface Command {
	public void apply();
}
