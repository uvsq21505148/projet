import java.util.Arrays;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;

import com.asprise.ocr.Ocr;


/**
 * Classe CommandAddFile permet d'ajouter un fichier a INBOX
*/
public class CommandAddFile implements Command {
	
	// Chemin absolu Linux vers le fichier
	private String chemin = "";
	private boolean option_inbox = false;
	
	/**
	 * Methode setChemin qui met à jours l'attribut chemin
	 * 
	 * @param chemin
	 * 		Vaut la nouvelle valeur du chemin
	 */
	public void setChemin (String chemin) {
		this.chemin = chemin;
	}
	
	public String getChemin() {
		return this.chemin;
	}
	
	/**
	 * Methode setOptionInbox qui met à jours l'attribut option_inbox
	 * 
	 * @param b
	 * 		Vaut true si l'option à été spécifié, false sinon
	 */
	public void setOptionInbox( boolean b ) {
		option_inbox = b;
	}
	
	public boolean getOptionInbox() {
		return option_inbox;
	}
	
	@Override
	public void apply () {
		AnalyseurArguments.argsCommandAddFile (this);
		
		// Ajout à INBOX : chemin absolu
		if ( option_inbox && ! chemin.equals("") && chemin.charAt(0) == '/') {
			
			try {
				// Récupération du contenu
				File file = new File(chemin);
				String contenu = ""; String type;
				
				type = Files.probeContentType(file.toPath());
				
				if ( type.equals("text/plain") ) {
					int c;
					FileReader in = new FileReader(file);
					do {
						c = in.read();
						if (c != -1) contenu += (char) c;
					}
					while( c != -1 );
				}
				else if ( type.equals("application/pdf") ) {
					Ocr.setUp();
					Ocr ocr = new Ocr();
					ocr.startEngine("fra", Ocr.SPEED_FASTEST);
					contenu = ocr.recognize(new File[] {file}, Ocr.RECOGNIZE_TYPE_ALL, Ocr.OUTPUT_FORMAT_PLAINTEXT);
					ocr.stopEngine();
				}
				
							
				// Création du fichier
				Fichier f = new Fichier
								.Builder( file.getName(), contenu )
								.build();
											
				// Ajout à INBOX
				Application.ENVIRONNEMENT.INBOX.add(f);
				Print.printWhiteln("Ajout de " + file.getName() + " à INBOX avec succès");
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		// Ajout au répertoire courant : nom du fichier
		else if ( ! option_inbox && ! chemin.equals("") && chemin.charAt(0) != '/') {
			
			Fichier f = Application.ENVIRONNEMENT.INBOX.getFichier(chemin);
			if( f != null ) {
				Application.ENVIRONNEMENT.courant.add(f);
				Print.printWhiteln("Ajout au dossier " + Application.ENVIRONNEMENT.path + " avec succès.");
			}
			else {
				Print.printWhiteln("Aucun fichier trouvé.");
			}
		}
		else {
			Print.printWhiteln("Aucun fichier trouvé.");
		}
	}
}
