import java.util.List;
import java.util.ArrayList;

/**
 * Classe CommandLS pour créer un répertoire dans le répertoire courant
*/
public class CommandMkdir implements Command {
	
	private String nom = "";
	
	public void setNom (String nom) {
		this.nom = nom;
	}
	
	@Override
	public void apply () {
		AnalyseurArguments.argsCommandMkdir (this);
		
		if ( ! nom.equals("") ) {
			Repertoire nouveauDossier = new Repertoire (
				nom,
				Application.ENVIRONNEMENT.courant
			);
			
			Application.ENVIRONNEMENT.courant.add (nouveauDossier);
			System.out.println("Répertoire créé avec succès.");
		}
		else {
			System.out.println("Aucun nom de dossier trouvé.");
		}
	}
}
