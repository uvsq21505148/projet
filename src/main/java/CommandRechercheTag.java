import java.util.ArrayList;
/**
 * Class CommandAjoutTag qui ajoute un tag à un fichier.
 * 
 */
public class CommandRechercheTag implements Command {
	private String tag;
	
	public CommandRechercheTag() {
		tag = new String();
	}
	
	/**
	 * Methode setTag qui met à jours l'attribut tag
	 * 
	 * @param s
	 * 		Vaut la nouvelle valeur du tag
	 */
	public void setTag(String s){
		this.tag = s;
	}
	
	/**
	 * Methode apply qui applique la commande.
	 */
	@Override
	public void apply() {
		AnalyseurArguments.argsCommandRechercheTag(this);
		
		for(Document d : Application.ENVIRONNEMENT.INBOX){
			Fichier f = (Fichier)d;
			
			if(f.containsTag(this.tag)){
				Print.printWhiteln("Le fichier "+f.getContenu()+" contient le tag "+ this.tag+"."); 
			}else{
				Print.printWhiteln("Aucun fichier ne contient le tag "+ this.tag+".");
			}
		}
	}
}
