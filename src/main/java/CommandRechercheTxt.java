import java.util.ArrayList;
/**
 * Class CommandRechercheTxt qui liste les fichier du répertoire courant contenant la chaine de caractère spécifié.
 * 
 */
public class CommandRechercheTxt implements Command {
	private String mot_cle;
	private boolean option_inbox;
	
	public CommandRechercheTxt() {
		mot_cle = new String();
		option_inbox = false;
	}
	
	public void setMotCle(String s){
		this.mot_cle = s;
	}
	
	public void setOptionInbox(boolean b) {
		option_inbox = b;
	}
	
	/**
	 * Methode apply qui applique la commande.
	 */
	@Override
	public void apply() {
		AnalyseurArguments.argsCommandRechercheTxt(this);
		
		if( option_inbox ) { 
			for(Document d : Application.ENVIRONNEMENT.INBOX){
				Fichier f = (Fichier) d;
			
				if(f.getContenu().contains(this.mot_cle)){
					Print.printWhite(f.getNom()+ " ");
				}
				Print.printWhiteln("");
			}
		}
		else {
			for(Document d : Application.ENVIRONNEMENT.courant){
				if(d instanceof Fichier) {
					Fichier f = (Fichier) d;
			
					if(f.getContenu().contains(this.mot_cle)){
						Print.printWhite(Application.ENVIRONNEMENT.path + f.getNom()+ " ");
					}
					Print.printWhiteln("");
				}
			}
		}
	}
}
