/**
 * Gestionnaire de Documents Personnels
 * */
public enum Application {
	ENVIRONNEMENT(
		"/", // Chemin de la racine
		new Repertoire("", null), // Répertoire racine
		new Repertoire("INBOX", null) // INBOX
	);
	
	public String path;
	public Repertoire courant;
	public Repertoire INBOX;
	
	private Application(String path, Repertoire courant, Repertoire INBOX) {
		this.path = path;
		this.courant = courant;
		this.INBOX = INBOX;
	}
	
	public static void main(String[] args) {
		ENVIRONNEMENT.run(args);
	}
	
	public void run(String[] args) {
		CommandFactory cf = CommandFactory.init();
		
		while(true) {
			cf.executeCommand(
				SaisieUtilisateur.getCommand());
		}
	}
}
