/**
 * Classe CommandQuit qui permet de quitter l'application.
 */
public class CommandQuit implements Command {
	
	private boolean option_joli; // option : "-joli"
	
	public CommandQuit() {
		option_joli = false;
	}
	
	/**
	 * Methode setOptionJoli qui indique sur le terminal la fin de l'execution.
	 * 
	 * @param b
	 *		Vaut true ou false si l'utilisateur à oui ou non spécifié cette option lors de la saisie.
	 */
	public void setOptionJoli(boolean b) {
		option_joli = b;
	}
	
	public boolean getOptionJoli(){
		return this.option_joli;
	}
	/**
	 * Methode apply qui applique la commande.
	 * 
	 */
	@Override
	public void apply() {
		AnalyseurArguments.argsCommandQuit(this);
		
		//Fermeture de l'application
		if(option_joli){
			Print.printBlue("Fermetur"); Print.printWhite("e de l'app"); Print.printRedln("lication.");
			Print.printBlue("Fermetur"); Print.printWhite("e de l'app"); Print.printRedln("lication.");
			Print.printBlue("Fermetur"); Print.printWhite("e de l'app"); Print.printRedln("lication.");
			Print.printBlue("Fermetur"); Print.printWhite("e de l'app"); Print.printRedln("lication.");
			Print.printBlue("Fermetur"); Print.printWhite("e de l'app"); Print.printRedln("lication.");
			Print.printBlue("Fermetur"); Print.printWhite("e de l'app"); Print.printRedln("lication.");
		}
		System.exit(0);
	}
}
