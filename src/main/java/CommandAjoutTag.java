import java.util.ArrayList;
/**
 * Class CommandAjoutTag qui ajoute un tag à un fichier.
 * 
 */
public class CommandAjoutTag implements Command {
	private String tag;
	private String fic;
	
	public CommandAjoutTag() {
		tag = new String();
		fic = new String();
	}
	
	public void setTag(String s){
		this.tag = s;
	}
	
	public void setFichier(String f){
		this.fic = f;
	}
	
	
	/**
	 * Methode apply qui applique la commande.
	 */
	@Override
	public void apply() {
		AnalyseurArguments.argsCommandAjoutTag(this);
		
		for(Document d : Application.ENVIRONNEMENT.INBOX){
			Fichier f = (Fichier)d;
			
			if(f.getNom().equals(this.fic)){
				f.addTag(this.tag);
				Print.printWhiteln("Le tag "+this.tag+" a été ajouté au fichier "+ this.fic+"."); 
			}else{
				Print.printWhiteln("Le fichier "+ this.fic+ " n\'existe pas.");
			}
		}
	}
}
