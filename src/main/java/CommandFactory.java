import java.util.Map;
import java.util.HashMap;
/**
 * Class CommandFactory qui stock toutes les commandes, les ajoutes et 
 * les executent.
 */
public class CommandFactory {
	
	private final Map<String, Command> commands;
	
	private CommandFactory() {
		commands = new HashMap<String, Command>();
	}
	
	/**
	 * Methode addCommand qui ajoute une nouvelle commande.
	 * 
	 * @param name
	 * 		Le nom de la commande ajouter.
	 * @param command
	 * 		L'objet command que l'on veut ajouter.
	 */
	public void addCommand(String name, Command command) {
		commands.put(name, command);
	}
	
	/**
	 * Methode executeCommand qui execute la commande passé en paramètre.
	 * 
	 * @param name
	 * 		Le nom de la command que l'on veut executer.
	 */
	public void executeCommand(String name) {
		if(commands.containsKey(name)) {
			commands.get(name).apply();
		}
	}
	
	/**
	 * Methode commandeExistante qui test si une commande du même nom existe déjà.
	 * 
	 * @param name
	 * 		Le nom de la commande que l'on veut tester.
	 * 
	 * @return true si la commande existe déjà, false sinon.
	 * 
	 */
	public boolean commandeExistante(String name) { return commands.containsKey(name); }

	/**
	 * Methode init qui ajoute toutes les commands.
	 * 
	 * @return L'objet CommandFactory avec toutes les commandes qui ont été ajoutées.
	 * 
	 */
	public static CommandFactory init() {
		CommandFactory cf = new CommandFactory();
		
		cf.addCommand("quit", new CommandQuit());
		cf.addCommand("ajouter", new CommandAjouter());
		cf.addCommand("addTag", new CommandAjoutTag());
		cf.addCommand("rechercheTag", new CommandRechercheTag());
		cf.addCommand("rechercher", new CommandRechercher());
		cf.addCommand("recherchetxt", new CommandRechercheTxt());
		cf.addCommand("cd", new CommandCD());
		cf.addCommand("mkdir", new CommandMkdir());
		cf.addCommand("ls", new CommandLS());
		cf.addCommand("addFile", new CommandAddFile());
		
		
		return cf;
	}
}
