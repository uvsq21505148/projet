import java.util.Scanner;
import java.util.Arrays;

/**
 * Class SaisieUtilisateur qui gère les saisies de l'utilisateur.
 */
public class SaisieUtilisateur {
		
	private static Scanner sc = new Scanner(System.in);
	private static String[] commande;
	
	/**
	 * Methode getCommand qui récupère la ligne saisie par l'utilisateur.
	 * 
	 * @return La commande saisie par l'utilisateur.
	 */
	public static String getCommand() {
		Print.printGreen(Application.ENVIRONNEMENT.path); Print.printWhite(" > ");
		commande = sc.nextLine().split(" ");
		
		//commande égale à la chaine vide "" si aucune commande en entrée
		return commande[0];
	}
	
	/**
	 * Methode getArgs qui récupère les arguments saisie par l'utilisateur.
	 * 
	 * @return La liste des arguments saisie par l'utilisateur.
	 */
	public static String[] getArgs() {
		//Tableau de taille 0 (tab.length == 0) si aucun argument
		return Arrays.copyOfRange(commande, 1, commande.length);
	}
}
