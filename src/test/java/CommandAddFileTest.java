import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;

public class CommandAddFileTest {

	@Test
	public final void testConstructor() {
		CommandAddFile cmd = new CommandAddFile();
		cmd.setChemin("/toto/titi");
		
		assertTrue("Methode setChemin de la class commandAddFile", cmd.getChemin() == "/toto/titi");
	}

	@Test
	public final void testSetOptionInbox() {
		CommandAddFile cmd = new CommandAddFile();
		cmd.setOptionInbox(true);
		assertTrue("Methode setOptionAuto de la class commandAddFile", cmd.getOptionInbox() == true);
	}

}
