import static org.junit.Assert.*;
import org.junit.Test;

public class CommandQuitTest {

	@Test
	public final void testConstructor() {
		CommandQuit cmd = new CommandQuit();
		
		assertTrue("Constructeur commandQuit", cmd.getOptionJoli() == false);
	}

	@Test
	public final void testSetOptionJoli() {
		CommandQuit cmd = new CommandQuit();
		cmd.setOptionJoli(true);
		assertTrue("Methode setOptionRJoli de la class commandQuit", cmd.getOptionJoli() == true);
	}
}
