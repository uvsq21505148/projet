import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;

public class CommandAjouterTest {

	@Test
	public final void testConstructor() {
		CommandAjouter cmd = new CommandAjouter();
		
		assertTrue("Constructeur commandAjouter", cmd.getOptionAuto() == false);
	}

	@Test
	public final void testSetOptionAuto() {
		CommandAjouter cmd = new CommandAjouter();
		cmd.setOptionAuto(true);
		assertTrue("Methode setOptionAuto de la class commandAjouter", cmd.getOptionAuto() == true);
	}

	@Test
	public final void testSetRepositories() {
		CommandAjouter cmd = new CommandAjouter();
		ArrayList<String> liste = new ArrayList<String>();
		
		cmd.setRepositories("fichier1");
		cmd.setRepositories("fichier2");
		
		liste.add("fichier1");
		liste.add("fichier2");
		
		assertTrue("Methode setRepositories de la class commandAjouter", cmd.getRepositories().equals(liste));
	}

	@Test
	public final void testSetFiles() {
		CommandAjouter cmd = new CommandAjouter();
		ArrayList<String> liste = new ArrayList<String>();
		
		cmd.setFiles("fichier1");
		cmd.setFiles("fichier2");
		
		liste.add("fichier1");
		liste.add("fichier2");
		
		assertTrue("Methode setFiles de la class commandAjouter", cmd.getFiles().equals(liste));
	}

}
