import static org.junit.Assert.*;
import org.junit.Test;

import java.util.ArrayList;

public class CommandRechercherTest {

	@Test
	public final void testConstructor() {
		CommandRechercher cmd = new CommandRechercher();
		
		assertTrue("Constructeur option_recursive commandRechercher", cmd.getOptionRecursive() == false);
		assertTrue("Constructeur option_inbox commandRechercher", cmd.getOptionInbox() == false);
		assertTrue("Constructeur option_tag commandRechercher", cmd.getOptionTag() == false);
	}

	@Test
	public final void testSetOptionRecursive() {
		CommandRechercher cmd = new CommandRechercher();
		cmd.setOptionRecursive(true);
		assertTrue("Methode setOptionRecursive de la class commandRechercher", cmd.getOptionRecursive() == true);
	}

	@Test
	public final void testSetOptionInbox() {
		CommandRechercher cmd = new CommandRechercher();
		
		cmd.setOptionInbox(true);
		
		assertTrue("Methode setOptionInbox de la class commandRechercher", cmd.getOptionInbox() == true);
	}
	
	@Test
	public final void testSetOptionTag() {
		CommandRechercher cmd = new CommandRechercher();
		
		cmd.setOptionTag(true);
		
		assertTrue("Methode setOptionTag de la class commandRechercher", cmd.getOptionTag() == true);
	}

	@Test
	public final void testAddTag() {
		CommandRechercher cmd = new CommandRechercher();
		ArrayList<String> liste = new ArrayList<String>();
		
		cmd.addTag("fichier1");
		cmd.addTag("fichier2");
		
		liste.add("fichier1");
		liste.add("fichier2");
		
		assertTrue("Methode addTag de la class commandRechercher", cmd.getTag().equals(liste));
	}

}
