# Gestionnaire de Documents Personnels

[Tuto markdown pour ecrire le readme](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

Ce projet s'inscrit dans l'UE Programmation, Génie Logiciel, et Preuve, et représente la moitié de la note de contrôle continu.

Le sujet est de construire une application permettant de gérer des documents personnels à la manière de Open Semantic Search ou Paperwork deux gestionnaires existants.

Notre application aura la différence d'être manipulée uniquement via une interface en ligne de commande, l'application devra également s'appuyer sur la gestion d'une base de données.








## Manuel Utilisateur








### Description de l'utilisation du logiciel.

Au lancement de l'application on se trouve dans le répertoire racine. Une exécution de ls nous permet de lister les hiérarchies virtuelles.

La navigation se fait comme dans un terminal linux. Des commandes similaires à cd, ls et mkdir sont disponibles.

```/chemin/absolu/repertoire/courant/ > on entrera la commande ici```

### Commandes disponibles

Entre crochets " [ arg ] " pour les arguments facultatifs.

Entre parenthèses et points de suspension " ( arg ... ) " pour une liste de taille indéterminée d'arguments.

* ```> mkdir nom```
	* Ajouter un dossier au répertoire courant.
	* ```nom``` : nom du dossier créé

* ```> ls -inbox || -virtual```
	* Afficher le contenu du répertoire courant.
	* -inbox : affiche tous les fichiers ajoutés à l'application
	* -virtual : affiche tous les fichiers du répertoire virtuel
	
* ```> cd nom```
	* Se déplacer dans un répertoire contenu dans le répertoire courant.
	* ```nom``` : nom du répertoire dans lequel se déplacer, ".." pour remonter au dossier parent
	
* ```> quit [-joli]```
	* Quitter l'application.
	* ```-joli``` : Affichage lors de l'exécution de la commande

* ```> addFile -inbox || -virutal chemin```
	* -inbox : ajout dans INBOX le fichier renseigné par le chemin absolu
	* -virtual : ajout dans la hiérarchie virtuel courante le fichier indiqué par le nom

* ```> recherchetxt -inbox || -virtual motcle```
	* Liste les fichiers contenant le mot clé recherché, pas de recherche récursive
	* ```motcle``` : chaine de caracteres à trouver


* ```> addTag -inbox || -virtual NomFichier tag```
	* Ajoute un tag à un fichier
	* ```tag``` : tag ajouté au fichier


* ```> rechercheTag -inbox || -virtual tag```
	* Recherche tout les fichiers contenant le tag spécifié
	* ```tag``` : tag recherché dans les fichier de l'INBOX


Pas implémentées complètement :

* ```> ajouter [-auto] chemin1 (chemin2 ...)```
	* Ajouter les fichiers indiqués par les chemins à l'INBOX
	* ```-auto``` : Classement (hiérarchie virtuelle et tags) et nommage automatique du fichier
	* ```chemin``` : 1 chemin de fichier ou répertoire minimum

* ```> rechercher [-inbox] ([tag=nom] ...) chaine```
	* Liste les fichiers du répertoire courant contenant la chaine de caractères ```chaine```.
	* ```-r``` : recherche récursive sur les répertoires contenu dans le répertoire courant.
	* ```-inbox``` : recherche sur l'intégralité des fichiers au lieu du répertoire courant.
	* ```-tag=nom``` : la recherche s'applique uniquement sur les fichiers contenant le tag ```nom```.
	







## Manuel Technique








### Tests unitaires

```gradle test```

Résultats à consulter dans e dossier ```build/test-results/```






### Interface en ligne de commande

```
	gradle fatJar
	java -jar build/libs/projet-all.jar
```

Cette instruction lance un invite de commande et attend les commandes décrites dans le manuel utilisateur.

#### Pattern Singleton

Le pattern est implémenté à l'aide d'une énumération sur Application. Il nous permet de ne créer qu'une instance de la classe appelée ENVIRONNEMENT lors de l'exécution du programme.

#### Pattern Factory

Class CommandFactory permet la création de commandes

#### Pattern Command

* Interface Command : interface fonctinnelle (Java 8)
* les classes implémentant l'interface (préfixées par Command) vont chacune implémenter une commande différente du manuel utilisateur.
* Classes AnalyseurArguments, SaisieUtilisateur : classes fonctionnelles permettant de traiter la lecture d'une commande et des arguments.
* Classe Application : Invoker, élément qui va appeler les commandes pour les exécuter.

#### Pattern Facade

Le pattern Facade sert à augmenter le niveau d'abstraction du code car un client va manipuler une class façade qui elle, instientiera les classes d'un sous-système.

L'utilisation de ce pattern permet aussi de forcer une utilisation précise d'un sous-système. Ainsi le client aura une interface moins complexe pour manipuler un ensemble de class appartenant à un ou plusieurs sous-systèmes.

Dans ce projet, l'utilisation d'une class façade Print.java permet aux autres classes d'utiliser plusieurs méthodes du module JAnsi grâce à une interface simple d'accès.









### Données de l'application

Les données manipulées sont concentrées dans deux classes ```Répertoire``` et ```Fichier```. Les types de fichier traités sont "text/plain" et "application/pdf". La reconnaissance de caractères est effectuée avec OCR pour les fichiers pdf.

#### Pattern Composite

```Répertoire``` et ```Fichier``` sont deux classes étendant la classe mère abstraite ```Document```.

On retrouve dans ```Repertoire``` une collection de ```Document```

#### Pattern Iterator

```Repertoire``` représente un ensemble de données, on peut donc utiliser le pattern Iterator pour parcourir cet ensemble.

Il suffit pour cela que notre classe implémente l'interface Iterable<Document> et redéfinisse la méthode Iterator<Document> iterator() de cette interface.

#### Persistence

Cette partie n'a pas été implémentée dans notre projet, un point majeur nous a posé problème.

La principale raison étant que le pattern Composite n'est pas très évident à manipuler avec une base de données, qui elle, se verrait facilitée son utilisation en séparant le composite en deux listes (une pour les fichiers et une pour les répertoires).

D'autre part, par un soucis de temps (l'implémentation des opérations CRUD et la table de jointure pour l'association ManyToMany Répertoire-Fichier étant longues), nous n'avons pas utilisé JDBC. Nous aurions voulu choisir un ORM et JPA, cela dit, le premier problème nous a empêché de faire fonctionner le code.

L'idée étant d'avoir les répertoires "courant" et "INBOX" de notre application persistants. Pour cela, il faut configurer le projet avec un fichier persistence.xml et ajouter les bonnes annotations aux classes Repertoire et Fichier, ces classes devront comprendre getters et setters pour tous les attributs et les objets seront initialisés au sein d'une transaction pour faire la correspondance avec la base de données.
